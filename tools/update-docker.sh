#!/bin/bash
#
# After run
#
#     $ docker-compose up --force-recreate --build
DOCKER_IMAGE="registry.gitlab.com/raniere-phd/radiomics"
docker image tag radiomics_web $DOCKER_IMAGE
docker push $DOCKER_IMAGE
