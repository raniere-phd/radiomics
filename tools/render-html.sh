#!/bin/bash
sed -i -e 's/self-contained: false/self-contained: true/' index.qmd
quarto render index.qmd --execute --to revealjs
